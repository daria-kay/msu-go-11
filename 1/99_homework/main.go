package main

import "fmt"

func ReturnInt() int {
  return 1
}

func ReturnFloat() float32 {
  return 1.1
}

func ReturnIntArray() [3]int {
  return [...]int {1, 3, 4}
}

func ReturnIntSlice() []int {
  return []int {1, 2, 3}
}

func IntSliceToString(slice []int) string {
  res := ""
  for _, value := range slice {
    res += fmt.Sprint(value)
  }
  return res
}

func MergeSlices(first []float32, second []int32) []int {
  res := make([]int, len(first) + len(second));
  for i, val := range first {
    res[i] = int(val)
  }
  for i, val := range second {
    res[i + len(first)] = int(val)
  }

  return res
}

func GetMapValuesSortedByKey(input map[int]string) []string {
  sortedKeys := make([]int, len(input))
  for key := range input {
    for pos, currentKey := range sortedKeys {
      if currentKey > key || currentKey == 0 {
        copy(sortedKeys[pos + 1:], sortedKeys[pos:])
        sortedKeys[pos] = key
        break
      }
    }
  }

  sortedValues := make([]string, len(input))
  for i, val := range sortedKeys {
    sortedValues[i] = input[val]
  }

  return sortedValues
}
